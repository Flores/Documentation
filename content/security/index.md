---
eleventyNavigation:
  key: Security
  title: Security
  icon: lock
  order: 50
---

This section contains information on how to securely use Codeberg.